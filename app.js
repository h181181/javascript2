//FROM TASK 4
// const http = require("http");

// const server = http
// 	.createServer((request, response) => {
// 		response.write("Hello Node World!");
// 		response.end();
// 	})
// 	.listen(8080);

// const server = express
// 	.createServer((request, response) => {
// 		response.write("Hello Express Site server");
// 		response.end();
// 	})
// 	.listen(8080);

//FOR TASK 5
const express = require("express");
var app = express();

var path = require("path");

app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname + "/index.html"));
});

app.get("/index.js", (req, res) => {
	res.sendFile(path.join(__dirname + "/index.js"));
});

app.listen(process.env.PORT || 8080);
